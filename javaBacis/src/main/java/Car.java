public class Car {

    private static final int WHEEL_AMOUNT = 4;

    private static String model = "";  //ten static jest niepotrzebny poniewaz jest zlem, kod nie jest juz obiektowy, a wszystkie samochody beda nosily nazwe ostatniego modelu wpisanego w Stringa i jeszcze nie zprintowanego
    private int year;

    public Car(String model, int year) {
        this.model = model;
        this.year = year;
    }

    public String getModel() {
        return model;
    }

    public int getYear() {

        return year;
    }

    public static void main(String[] args) {
        System.out.println(args.length);
        Car bmw = new Car("x1", 2017);
        Car audi = new Car("a5", 2015);

        bmw.model = "fiat";
        Car.model = "fiat"; //zmieniamy zmienna wywolujac na rzec klasy a nie obiektu

        System.out.println(bmw.getModel());
        System.out.println(audi.getModel());
    }
}
