public class Date {

    private static String[] monthNames;

    static {
        if(1>2)
            monthNames = new String[10];
    }

    private int year;
    private int month = 12;

    public static void main(String[] args) {
        Date date = new Date();
        date.nextMonth();
    }

    public void nextMonth() {
        if (month == 12) {
            year++;
            month = 1;
        } else month++;
    }
}
