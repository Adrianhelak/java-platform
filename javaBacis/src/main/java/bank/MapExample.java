package bank;

import java.util.HashMap;
import java.util.Map;

public class MapExample {
    public static void main(String[] args) {
        Map<Long, User> useridToUser = new HashMap<>();

        useridToUser.put(1l, new User("Kowalski"));
        useridToUser.put(1l, new User("Michalski"));
        useridToUser.put(3l, new User("Gegalski"));

        User bankUser = useridToUser.get(3l);


        //iterujemy petla po wszystkich kluczach.
        //taka petla po kluczach pozwala dostac sie bardzo szbko do wartosci iterujac po mapie
        for (Long aLong : useridToUser.keySet()) {
            useridToUser.get(aLong);
        }

        for (User user : useridToUser.values()) {
            System.out.println(user);
        }

        for (Map.Entry<Long, User> oneEntry : useridToUser.entrySet()) {
            oneEntry.getKey();
            oneEntry.getValue();
        }

        for (Long id : useridToUser.keySet()) {
            System.out.println("id " + id +" user" + useridToUser.get(id));
        }

        Map<User, Long> userToId = new HashMap<>();
        User jan = new User("jan");
        User carl = new User("carl");
        User carl2 = new User("carl");

        userToId.put(jan, 1l);
        userToId.put(carl, 1l);
        userToId.put(carl2, 1l);

    }
}
