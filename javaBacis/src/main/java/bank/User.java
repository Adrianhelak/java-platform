package bank;

import java.util.Objects;

public class User {

    private String name;
    private String surname;
    private String login;
    private int pesel;
    private int age;

    public User(String name) {
        this.name = name;
    }
    @Override
    public String toString(){
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return pesel == user.pesel &&
                Objects.equals(name, user.name) &&
                Objects.equals(surname, user.surname) &&
                Objects.equals(login, user.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, login, pesel);
    }
}
