package threads;

public class MainSecondExample {
    private static Integer counter = 0;

    public static void main(String[] args) throws InterruptedException {

        Runnable r = new Runnable() {
            @Override
            public void run() {
                //pozwala nam na bezpieczne korzystanie ze zmiennej counter miedzy dwoma nawiasami
                synchronized (counter) {
                    counter++;
                }
            }
        };

        Thread t = new Thread(r);
        t.start();
        Thread t2 = new Thread(r);
        t2.start();
        t.join();
        t2.join();
        System.out.println(counter);
        System.out.println("finish");
    }
    public synchronized void increment(){
        counter++;
    }
}
