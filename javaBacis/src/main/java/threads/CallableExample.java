package threads;

import java.util.concurrent.*;

import static java.util.concurrent.TimeUnit.SECONDS;

public class CallableExample {

    ExecutorService executor = Executors.newSingleThreadExecutor();

    public static void main(String[] args) throws Exception{
        new CallableExample().start();

    }

    private void start() throws Exception {
        Callable<String> call = new Callable<String>() {
            @Override
            public String call() throws Exception {
                Thread.sleep(15000);
                return "test";
            }
        };
        Future<String> future = executor.submit(call);
        String result= future.get(10, SECONDS);
    }
}
