package carExercise;

public class Car {
    private static final int WHEEL_AMOUNT = 4;

    private String model = "";
    private int year;

    public Car(String model, int year) {
        this.model = model;
        this.year = year;
    }

    public String getModel() {
        return model;
    }

    public int getYear() {

        return year;
    }

    public boolean equals(Object o) {
        Car c = (Car) o;
       return model.equals(c.model) && year == c.year;
    }
}

