package obiektimmutable;

public class MyTime {
    private int hour;
    private int minute;


    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public MyTime(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
    }

    public MyTime nextHour() {
        return new MyTime(hour + 1, minute);
    }

    public MyTime nextMinute() {
        return new MyTime(hour, minute + 1);
    }
}
