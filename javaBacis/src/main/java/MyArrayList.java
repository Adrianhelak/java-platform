@SuppressWarnings("Unchecked")
public class MyArrayList<T> {
    private static final int INT = 10;
    //add get obiekt po i, size, index of, remove
    private T[] elements = (T[])new Object[INT];
    private int size = 0;

    public void addStringElement(T element) {
        size++;
    }

    private void ensureCapacity() {
        if (size < elements.length) return;

        T[] copy = (T[])new Object[elements.length * 2];
        for (int i = 0; i < elements.length; i++) {
            copy[i] = elements[i];
        }
        elements = copy;
    }

    public int SizeOfArray() {
        return size;
    }

    public int indexOfArray(Object o) {
        for (int i = 0; i < size; i++)
            if (o == elements[i])
                return i;
        throw new IllegalArgumentException("Element not found");
    }

    public T getElementI(int i) {
        if (i > size)
            throw new IndexOutOfBoundsException();
        return elements[i];
    }

    public T removeLastElement() {
        T removedElement = elements[size - 1];
        elements[size - 1] = null;
        size--;
        return removedElement;
    }

    public void subList(T[] subElements, int indexFrom, int indexTo) {
        int index = 0;
        for (int i = indexFrom; i < indexTo; i++) {
            subElements[index] = elements[i];
            index++;
        }
    }
}
