import obiektimmutable.MyTime;

import java.util.ArrayList;

public class TestGeneric<T extends MyTime, K, V> {

    private T value;
    private K key;
    private V other;

    private ArrayList<T> elements;

    private ArrayList<? extends Car> vehicles;

    public TestGeneric(T value) {
        this.value = value;
    }

    public T some() {
        return null;
    }

    public void some(T t) {

    }

}
