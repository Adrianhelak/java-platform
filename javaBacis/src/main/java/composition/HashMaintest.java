package composition;

import java.util.HashSet;

public class HashMaintest {
    public static void main(String[] args) {
        HashSet<String> names = new HashSet<String>();

        names.add("aab");
        names.add("jsd");
        names.add("ads");

        System.out.println(names.size());

        for (String name : names) {
            System.out.println(name);
        }

        HashSet<Person> persons = new HashSet<Person>();
        persons.add(new Person("jan",20));
        persons.add(new Person("jan",20));
        persons.add(new Person("jan",20));

        System.out.println(persons.size());
    }
}
