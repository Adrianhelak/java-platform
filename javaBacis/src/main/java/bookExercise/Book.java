package bookExercise;

import java.util.Arrays;
import java.util.List;

public class Book {

    private String title;
    private int price;
    private int isbn;
    private List<Author> authors;

    public Book(Author author, String title) {
        this.authors = Arrays.asList(author);
        this.title = title;
    }

    public Book(Author author, String title, int isbn) {
        this.authors = Arrays.asList(author);
        this.title = title;
        this.isbn = isbn;
    }

    public Book(Author author, String title, int price, int isbn) {
        this.authors = Arrays.asList(author);
        this.title = title;
        this.price = price;
        this.isbn = isbn;
    }

    public List<Author> getAuthors(){
        return authors;
    }

    public String getTitle() {
        return title;
    }

    public int getPrice() {
        return price;
    }

    public int getIsbn() {
        return isbn;
    }


}
