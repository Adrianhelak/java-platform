package singleton;

public class DbConnetionBuilderAlter {
    private DbConnection connection = new DbConnection();

    public DbConnetionBuilderAlter withUrl(String url) {
        connection.setUrl(url);
        return this;
    }

    public DbConnetionBuilderAlter withLogin(String login) {
        connection.setLogin(login);
        return this;
    }

    public DbConnetionBuilderAlter withPassword(String password) {
        connection.setPassword(password);
        return this;
    }

    public DbConnetionBuilderAlter withMode(String mode) {
        connection.setMode(mode);
        return this;
    }

    public DbConnetionBuilderAlter withPort(int port) {
        connection.setPort(port);
        return this;
    }
    public DbConnection build (){
        return connection;
    }
}
