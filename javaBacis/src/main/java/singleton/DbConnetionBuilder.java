package singleton;

public class DbConnetionBuilder {
    private String url;
    private String login;
    private String password;
    private String mode;
    private int port;

    public DbConnetionBuilder withUrl(String url) {
        this.url = url;
        return this;
    }

    public DbConnetionBuilder withLogin(String login) {
        this.url = url;
        return this;
    }

    public DbConnetionBuilder withPassword(String password) {
        this.url = url;
        return this;
    }

    public DbConnetionBuilder withMode(String mode) {
        this.mode = mode;
        return this;
    }

    public DbConnetionBuilder withPort(int port) {
        this.port = port;
        return this;
    }
    public DbConnection build (){
        DbConnection connetion = new DbConnection();
        connetion.setLogin(login);
        connetion.setPassword(password);
        connetion.setUrl(url);
        connetion.setPort(port);
        connetion.setMode(mode);
        return connetion;
    }
}
