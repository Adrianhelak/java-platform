package compare;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CarComparator {
    public static void main(String[] args) {
        Car c1 = new Car("audi", "c1", 2,4);
        Car c2 = new Car("audi", "b2", 3,4);
        Car c3 = new Car("audi", "b2", 3,5);
        Car c4 = new Car("bmw", "x2", 2,4);
        
        List<Car> cars = Arrays.asList(c1,c2,c3,c4);
        System.out.println("---before sort---");
        for (Car car : cars) {
            System.out.println(car);
        }

        Collections.sort(cars);

        System.out.println("\n---after sort---");
        for (Car car : cars){
            System.out.println(car);
        }
    }
}
