package compare;

public class Car implements Comparable<Car>{

    private String make;
    private String model;
    private int year;
    private int purchaseYear;

    public Car(String make, String model, int year, int purchaseYear) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.purchaseYear = purchaseYear;
    }

    public int compareTo(Car c) {
        if (make.compareTo(c.make) == 0)
            return make.compareTo(c.make);
        if (!model.equals(c.model))
            return model.compareTo(c.model);
        if (year !=c.year)
            return year-c.year;
        return purchaseYear - c.purchaseYear;
    }

    @Override
    public String toString() {
        return "Car{" +
                "make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", purchaseYear=" + purchaseYear +
                '}';
    }
}
