package compare;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CompareMain {
    public static void main(String[] args) {
        Person p1 = new Person("a", "a", 30);
        Person p2 = new Person("b", "a", 10);
        Person p3 = new Person("a", "z", 20);

        List<Person> persons = Arrays.asList(p1, p2, p3);
        System.out.println("---before sort---");
        for (Person person : persons) {
            System.out.println(person);
        }

        Collections.sort(persons , new AgeComparator());

        System.out.println("---after sort---");
        for (Person person : persons){
            System.out.println(person);
        }
    }
}
