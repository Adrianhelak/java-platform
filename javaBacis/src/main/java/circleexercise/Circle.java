package circleexercise;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Circle {
    private int x;
    private int y;
    private int r;

    public Circle(int x, int y, int r) {
        this.x = x;
        this.y = y;
        this.r = r;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getR() {return r;}

    public int[] getXAndY() {
        return new int[]{x, y};
    }

    public double calculateArea(int r){
        return 3.14*r*r;
    }

}
